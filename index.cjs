const assert = require('assert')
const {createWriteStream} = require('fs')
const {Readable, pipeline} = require('stream')
const {promisify} = require('util')

const pipe = promisify(pipeline)

const inputFile = process.env.PARL_INPUT_FILE
assert(inputFile, new ReferenceError('Please set PARL_INPUT_FILE environment variable'))

Promise.all([
    import('./csv.mjs'),
    import('./extractRepresentatives.mjs'),
    import('./validateRepresentative.mjs'),
]).then(([
    {default: csvRowFrom},
    {default: extractRepresentatives},
    {default: validateRepresentative},
]) => {

    const representatives = extractRepresentatives(require('./' + inputFile))

    const errors = representatives
        .map(validateRepresentative)
        .filter(_ => _);

    if (errors.length > 0) {
        errors.forEach(console.error);
        throw new Error(`${errors.length} invalid rows (see console error messages above for details)`);
    }

    console.log('Successfully parsed %d representatives!', representatives.length);

    const filepath = `./dist/${Date.now()}_circonscription.csv`

    pipe(Readable.from(representatives.map(csvRowFrom)), createWriteStream(filepath))
        .then(() => console.log('Successfully wrote data to %s', filepath))
        .catch(err => {
            console.error('Unable to write data to %s because %s', filepath, err);
            process.exitCode = 1;
        });

})

