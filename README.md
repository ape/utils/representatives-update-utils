# Utility to update data on representatives.apeorg.com

## Prerequisites

- Node.js v16

## Installation

Clone this repository and run `npm install`.

## Usage

### From remote dataset ([nosdeputees.fr](https://github.com/regardscitoyens/nosdeputes.fr/blob/master/doc/api.md#liste-des-parlementaires))


Set the `PARL_DATA_ORIGIN` environment variable and run the **MJS** script.

```shell
PARL_DATA_ORIGIN=https://www.nosdeputes.fr/deputes/enmandat/json node index.mjs 
```

### From a local JSON file

Set the `PARL_INPUT_FILE` environment variable and run the **CJS** script.

```shell
PARL_INPUT_FILE=my_json_file_path.json node index.cjs 
```