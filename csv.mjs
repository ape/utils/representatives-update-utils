import {EOL} from 'os'

export default function csvRowFrom(obj) {
    return Object
        .values(obj)
        .map(makeRowWithDoubleQuotes)
        .join(', ')
        .concat(EOL)
}

function makeRowWithDoubleQuotes(row) {
    return row ? `"${escapeDoubleQuotes(row)}"` : '""'
}

function escapeDoubleQuotes(input) {
    return String(input).replaceAll('"', '""')
}