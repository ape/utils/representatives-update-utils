export default function extractRepresentatives(json) {
    return json.deputes.map(
        ({
             depute: {
                 nom_de_famille: lastName,
                 prenom: firstName,
                 sexe: genre,
                 num_deptmt: departmentNumber,
                 num_circo: districtNumber,
                 // nom_circo: districtName,
                 // groupe_sigle: parlGroup,
                 // parti_ratt_financier: party,
                 sites_web: websites,
                 emails,
                 twitter,
                 // place_en_hemicycle: seatNumber,
                 // url_an: urlAssembleeNationale,
                 // id_an: idAssembleeNationale,
                 // url_nosdeputes: urlNosDeputes,
                 // slug,
             }
         }) => ({
            districtINSEE: `${departmentNumber}-${districtNumber}`,
            title: genre === 'F' ? 'Mme' : 'M',
            firstName,
            lastName: (lastName.endsWith(')') ? lastName.replace(/^(.+)\(([a-z]+)\)$/i, '$2 $1') : lastName).trim().toUpperCase(),
            email: emails.map(_ => _.email).find(_ => !!_ && _.toLowerCase().includes('assemblee-nationale.fr')) || emails[0]?.email || undefined,
            phone: undefined,
            twitter: twitter ? `https://twitter.com/${twitter}` : undefined,
            facebook: websites.map(_ => _.site).find(_ => !!_ && _.toLowerCase().includes('facebook.com')),
        })
    )
}
