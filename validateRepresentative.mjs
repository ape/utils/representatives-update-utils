export default function validateRepresentative({districtINSEE, firstName, lastName}, index) {
    if (typeof districtINSEE !== 'string') {
        return new TypeError(`Entry #${index} (${lastName} ${firstName}) has an invalid INSEE code (districtINSEE MUST be a string)`);
    }

    const pattern = /^(9?\d{2}|2[AB])-\d{1,2}$/
    if (!pattern.test(districtINSEE)) {
        return new RangeError(`Entry #${index} (${lastName} ${firstName}) has an invalid INSEE code (districtINSEE MUST match “<Department’s COG>-<District’s number>”, “${districtINSEE}” given)`);
    }

    return null;
}