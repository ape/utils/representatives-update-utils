import assert from 'assert'
import {createWriteStream} from 'fs'
import {pipeline, Readable} from 'stream'
import {promisify} from 'util'
import fetch from 'node-fetch'
import csvRowFrom from './csv.mjs'
import extractRepresentatives from './extractRepresentatives.mjs'
import validateRepresentative from './validateRepresentative.mjs';

const pipe = promisify(pipeline)

// Example: https://www.nosdeputes.fr/deputes/enmandat/json
const origin = process.env.PARL_DATA_ORIGIN
assert(origin, new ReferenceError('Please set PARL_DATA_ORIGIN environment varialbe'))

const start = process.hrtime.bigint()

const representatives = await fetch(origin, {headers: {'Accept': 'application/json'}})
    .then(async response => {
        const duration = Number(BigInt.asIntN(32, (process.hrtime.bigint() - start) / BigInt(1000 * 1000))) / 1000;
        console.log('Successfully downloaded data in %d seconds...', duration)
        if (response.ok) return response.json()
        throw new Error(await response.text())
    })
    .then(extractRepresentatives)
    .catch((err) => {
        console.error('Error while fetching representatives: %s', err.message || err)
        process.exit(1)
    })

const errors = representatives
    .map(validateRepresentative)
    .filter(_ => _);

if (errors.length > 0) {
    errors.forEach(console.error);
    throw new Error(`${errors.length} invalid rows (see console error messages above for details)`);
}

console.log('Successfully parsed %d representatives!', representatives.length);

const filepath = `./dist/${Date.now()}_circonscription.csv`

try {
    await pipe(Readable.from(representatives.map(csvRowFrom)), createWriteStream(filepath))
    console.log('Successfully wrote data to %s', filepath)
}
catch (err) {
    console.error('Unable to write data to %s because %s', filepath, err);
    process.exitCode = 1;
}


